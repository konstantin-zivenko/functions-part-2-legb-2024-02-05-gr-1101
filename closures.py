def power_factory(exp):
    def power(base):
        return base ** exp

    return power


def mean():
    sample = []

    def _mean(number):
        sample.append(number)
        return sum(sample) / len(sample)

    return _mean


def mean_v2():
    total = 0
    length = 0

    def _mean(number):
        nonlocal total, length
        total += number
        length += 1
        return total / length

    return _mean


if __name__ == "__main__":
    cases = [
        (10, 10),
        (11, 10.5),
        (12, 11)
    ]
    mean_current = mean_v2()
    for arg, res in cases:
       assert mean_current(arg) == res, f"ERROR! mean_current({arg}) expected {res}, but got {mean_current(arg)}"
